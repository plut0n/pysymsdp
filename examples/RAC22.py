import qutip as Q
import numpy as np
from itertools import product
from pysymsdp import NVProblem
import pysymsdp.Random.Random as rnd

class RAC222(NVProblem):
    def __init__(self):
        super().__init__()
        self.forceReal = True

    def sampleOperators(self):
        dim = 2
        X = [rnd.pureNormalizedDensityMatrix(dim) for _ in range(4)]
        U = [rnd.unitary(2) for _ in range(2)]
        M = [np.array([[1,0],[0,0]]), np.array([[0,0],[0,1]])]
        X += [u@m@u.conj().T for u in U for m in M]
        return X

    def operatorTypes(self):
        types = [list(range(4)), list(range(4,8))]
        return types

    def sampleStateKraus(self):
        dim = 2
        K = Q.identity(dim)
        return K

    def computeObjective(self, X, K):
        obj = 0
        for x1,x2,y in product(range(2), repeat=3):
            b = x1 if y == 1 else x2
            rho = X[x1+2*x2]
            M = X[4+b+2*y]
            obj += (M*rho).tr()
        obj /= (obj/8).real
        return obj

    def symmetryGroupGenerators(self):
        swapX1X2 = [0,2,1,3,6,7,4,5]
        flipX1 = [1,0,3,2,5,4,6,7]
        generators = [swapX1X2, flipX1]
        return generators

    def ambientGroupGenerators(self):
        generators = [[1, 2, 3, 0, 4, 5, 6, 7],
                [1, 0, 2, 3, 4, 5, 6, 7],
                [0, 1, 2, 3, 5, 4, 6, 7],
                [0, 1, 2, 3, 6, 7, 4, 5]]
        return generators

    def operatorSDPConstraints(self, X):
        C = X
        return C

    def operatorEqualityConstraints(self, X):
        dim = 2
        C = [np.eye(dim)-X[4]-X[5], np.eye(dim)-X[6]-X[7]]
        return C

    def scalarEqualityConstraints(self, X):
        dim = 2
        mm = Q.identity(dim)/dim
        C = [mm-X[_] for _ in range(4)]
