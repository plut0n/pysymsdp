import numpy as np
from abc import ABCMeta, abstractmethod

class NVProblem(object, metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        self.cacheNumOperators = []
        self.forceReal = False

    @abstractmethod
    def sampleOperators(self):
        raise NotImplementedError("Please implement this method")

    @abstractmethod
    def sampleStateKraus(self):
        raise NotImplementedError("Please implement this method")
    
    @abstractmethod
    def computeObjective(self,X,K):
        raise NotImplementedError("Please implement this method")
        
    def operatorTypes(self):
        return np.zeros((self.numOperators,1))

    def symmetryGroupGenerators(self):
        return np.arange(self.numOperators)

    def ambientGroupGenerators(self):
        return np.arange(self.numOperators)

    def numOperators(self):
        if self.cacheNumOperators is []:
            self.cacheNumOperators = length(self.sampleOperators)
        return self.cacheNumOperators

    def operatorSDPConstraints(self, X):
        return []

    def operatorEqualityConstraints(self, X):
        return []

    def scalarEqualityConstraints(self, X):
        return []
