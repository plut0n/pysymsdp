class NVSettings:
    def __init__(self, **kwargs):
        self.picosSettings = kwargs.get("picosSettings",[])
        self.sampleChunkSize = kwargs.get("sampleChunkSize",20)
        self.checkLevel = kwargs.get("checkLevel",1)
        self.numSampleCheck = kwargs.get("numSampleCheck",10)
        self.verbosityLevel = kwargs.get("verbosityLevel",1)
        self.checksEigTol = kwargs.get("checksEigTol",1e-9)
        self.checksTol = kwargs.get("checksTol",1e-12)
        self.checksObjTol = kwargs.get("checksObjTol",1e-9)
        self.monomialTol = kwargs.get("monomialTol",1e-12)
        self.monomialHist = kwargs.get("monomialHist",[])
        self.findSymTol = kwargs.get("findSymTol",1e-12)
        self.finSymHist = kwargs.get("findSymHist",[])
        self.blockDiagRefine = kwargs.get("blockDiagRefine",True)
        self.blockDiagOrbits = kwargs.get("blokDiagOrbits",True)
        self.blockDiagEigTol = kwargs.get("blockDiagEigTol",1e-9)
        self.blockDiagEigHist = kwargs.get("blockDiagEigHist",[])
        self.blockDiagMatTol = kwargs.get("blocDiagMatTol",1e-9)
        self.blockDiagMatHist = kwargs.get("blockDiagMatHist",[])

    def log(self, message, level=1):
        if self.verbosityLevel >= level:
            print(message)

    def plotHistograms(self):
        raise NotImplemented("Not Implemented")

    @staticmethod
    def picosMOSEK(tolerance, **kwargs):
        picosSettings = {'solver': 'mosek', 'mosek_params': {
            'MSK_DPAR_INTPNT_CO_TOL_PFEAS': tolerance,
            'MSK_DPAR_INTPNT_CO_TOL_DFEAS': tolerance,
            'MSK_DPAR_INTPNT_CO_TOL_REL_GAP': tolerance,
            'MSK_DPAR_INTPNT_CO_TOL_INFEAS': tolerance,
            'MSK_DPAR_INTPNT_CO_TOL_MU_RED': tolerance,
            'MSK_DPAR_INTPNT_TOL_PFEAS': tolerance,
            'MSK_DPAR_INTPNT_TOL_DFEAS': tolerance,
            'MSK_DPAR_INTPNT_TOL_INFEAS': tolerance,
            'MSK_DPAR_INTPNT_TOL_REL_GAP': tolerance}
            **kwargs}
        return picosSettings

    #TODO: Implement settings for other solvers.
