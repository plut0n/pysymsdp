from . import GenPerm

class Check():
    @staticmethod
    def problem(pb, settings):
        Check.samples(pb, settings)
        Check.ambientGroup(pb, settings)
        Check.symmetryGroup(pb, settings)

    @staticmethod
    def samples(pb, settings):
        X = pb.sampleOperators()
        K = pb.sampleStateKraus()
        Check.sampleObeysConstraints(pb, X, K, settings)

    @staticmethod
    def symmetryGroup(pb, settings):
        Check.generatorsLeaveOperatorsFeasible(pb, pb.symmetryGroupGenerators,
                settings)
        Check.generatorsLeaveObjectiveInvariant(pb, pb.symmetryGroupGenerators, 
                settings)

    @staticmethod
    def ambiantGroup(pb, settings):
        Check.generatorsLeaveOperatorsFeasible(pb, pb.ambientGroupGenerators,
                settings)

    @staticmethod
    def generatorsLeaveObjectiveInvariant(pb, generators, settings):
        X = pb.sampleOperators()
        K = pb.sampleStateKraus()
        obj = pb.computeObjective(X, K)
        for g in generators:
            X_ = GenPerm.operatorsImage(g, X)
            obj_ = pb.computeObjective(X_, K)
            diff = abs(obj_ - obj)
                assert diff < settings.checksTol, (
                        "The generator\n {}\n does not preserve the objective".{g})
        
    @staticmethod
    def generatorsLeaveOperatorsFeasible(pb, generators, settings):
        X = pb.sampleOperators()
        K = pb.sampleStateKraus()
        for g in generators:
            Check.sampleObeysConstraints(problem, GenPerm.operatorsImage(g, X),
                    K, settings, "For generator {}".format(g))

    @staticmethod
    def sampleObeysConstraints(pb, X, K, settings, message):
        Csdp = pb.operatorSDPConstraints(X)
        CopEq = pb.operatorEqualityConstraints(X)
        CscEq = pb.scalarEqualityConstraints(X)
        for i,sdp in enumerate(Csdp):
            mineig = min(sdp.eigenenergies())
            assert mineig > -settings.checkEigTol, (
                    "{} SDP constraint {} violated, minimal eigenvalue {}".format(
                        message, i, mineig))
        for i,opEq in enumerate(CopEq):
            maxAbsSingVal = max(abs(opEq.eigeneregies(())))
            assert maxAbsSingVal < settings.ChecksEigTol, (
                    "{} op. equ. constraints {} violated, maximal singular value {}".format(
                        message, i, maxAbsSingVal))
        for i,scEq in enumerate(CscEq):
            diff = abs((K.dag()*scEq*K).tr())
            assert diff < settings.checkTol, (
                    "{} sc. equ. constraints {} violated, difference {}".format(
                        message, i, diff))
