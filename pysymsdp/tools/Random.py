import numpy as np

CAXIS = np.array([1,1j])

class Random():
    @staticmethod
    def unitary(dim):
        """ Returns a random unitary matrix of dimension `dim`

        Parameters
        ----------
        dim : int
            Dimension of the unitaries to generate.

        Returns
        -------
        U : nd-array
            Unitary matrix of size `dim`x`dim`.
        """
        gin = Random.complexGaussian(dim)
        q,r = np.linalg.qr(gin)
        l = np.diag(r).copy() #copy() since diag is read-only
        l /= np.abs(l)
        U = q*l
        return U

    @staticmethod
    def realGaussian(dim, sigma=1):
        m = np.random.randn(dim,dim)*sigma
        return m

    @staticmethod
    def complexGaussian(dim, sigma=1):
        m = np.sum(np.random.randn(*((dim,)*2+(2,)))*CAXIS, 
                axis=-1)*(sigma/np.sqrt(2))
        return m

    @staticmethod
    def hermitianGaussian(dim, sigma=1):
        m = Random.complexGaussian(dim, sigma)
        m = .5*(m+m.conj().H)
        return m

    @staticmethod
    def symmetricGaussian(dim, sigma=1):
        core = Random.realGaussian(dim, sigma)
        #This works since all element of M are from the same distribution
        m = np.tril(core) + np.tril(core, -1).T   
        return m

    @staticmethod
    def unormalizedPureState(dim):
        ket = np.sum(np.random.rand(dim,2)*CAXIS, axis=-1)[:, None]
        return ket

    @staticmethod
    def normalizedPureState(dim):
        ket = Random.unormalizedPureState(dim)
        ket /= np.linalg.norm(ket)
        return ket

    @staticmethod
    def pureNormalizedDensityMatrix(dim):
        ket = Random.unormalizedPureState(dim)
        rho = ket@ket.T
        rho /= rho.trace()
        return rho
