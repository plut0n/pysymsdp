import numpy as np

from random import random
from sympy.combinatorics import Permutation
from sympy.combinatorics.perm_groups import PermutationGroup

class GenPerm():
    """Methods to handle generalized permutations.
    """
    @staticmethod
    def closure(generators):
        """Compute the closure of a set of generators using Dimino's algorithm.
        
        Parameters
        ----------
        generators : ndarray
            Set of generators. 2d-array, each line being generator.

        Returns
        -------
        G : ndarray
            Closure of the set of provided generators.

        """
        a = [Permutation(g) for g in generators]
        permGroup = PermutationGroup(a)
        G = list(permGroup.generate_dimino(af=True))
        return np.array(G)

    @staticmethod
    def randomFormChain(genPermChain):
        """Given a group decomposition, return a random element.

        Parameters
        ----------
        genPermChain : ndarray
            Group decomposition, array of permutations (array).

        Returns
        -------
        g : array
            A random element from genPermChaim.
        
        """
        for c in genPermChain:
            ui = c[int(random()*c.shape[0])]
            g = GenPerm.compose(g, ui)
        return g

    @staticmethod
    def symmetrize(mat, genPermChain):
        """Symmetrizes a matrix under the action of a symmetry group (jointly
        on the rows and column). 

        Parameters
        ----------
        mat : nd-array
            Matrix to symmetrize.

        genPermChain : nd-array
            Group decomposition, array of permutations (array).

        Returns
        -------
        matSym : nd-array
            Symmetrized matrix `m̀at` under `genPermChain`.

        """
        N = mat.shape[0]
        matSym = mat.copy()
        for u in genPermChain:
            av = np.zeros((N,N))
            for c in u:
                #Debug purpose, -> when test check if c is on dim 1
                if len(c.shape)==1:
                    print("Remove debug ok")
                else:
                    print("You fucked up here")
                absc = np.abs(c)
                mS = np.where(c<0)[0]
                if mS.size>0:
                    pS = np.where(c>0)[0]
                    pT = absc[ps]
                    mT = absc[mS]
                    av[np.ix_(pT,pT)] += matSym[np.ix_(pS,pS)]
                    av[np.ix_(mT,mT)] += matSym[np.ix_(mS,mS)]
                    av[np.ix_(pT,mT)] -= matSym[np.ix_(pS,mS)]
                    av[np.ix_(mT,pT)] -= matSym[np.ix_(mS,pS)]
                else:
                    av += matSym
            av /= u.shape[0]
            matSym = av.copy()
        return matSym

        @staticmethod
        def slowSymmetrize(mat, genPermChain):
            """Symmetrizes a matrix under the action of a symmetry group (jointly
            on the rows and column). Slow method, cf. `GenPerm.symmetrize`.

            Parameters
            ----------
            mat : nd-array
                Matrix to symmetrize.

            genPermChain : nd-array
                Group decomposition, array of permutations (array).

            Returns
            -------
            matSym : nd-array
                Symmetrized matrix `m̀at` under `genPermChain`.

            """
            matSym = mat.copy()
            N = mat.shape[0]
            for c in genPermChain[::-1]:
                av = np.zeros((N,N))
                for e in c:
                    av += GenPerm.matrixImage(e, matSym)
                av /= c.shape[0]
                matSym = av.copy()
            return matSym

        @staticmethod
        def vectorImage(gp, v):
            """Image of a vector under a group element.

            Parameters
            ----------
            gp : array
                Group element.
            v : array
                Vector.

            Returns
            -------
            iv : array
                Image of `v` under `gp`.

            """
            v *= np.sign(gp)
            iv = v[np.abs(gg)]
            return iv

        @staticmethod
        def orthogonalMatrix(gp):
            """Permutation/orthogonal matrix corresponding to a group element.

            Parameters
            ----------
            gp : array
                Group element.

            Returns
            -------
            om : ndarray
                Orthogonal matrix correspoding to `gp`.
            """
            m = np.diag(np.sign(gp))
            om = m[np.abs(gp)]
            return om

        @staticmethod
        def slowOrthogonalMatrix(gp):
            """Coresponding to the definition given in 
            `10.1103/PhysRevLett.122.070501`.

            Parameters
            ----------
            gp : array
                Group element.

            Returns
            -------
            om : ndarray
                Orthogonal matrix coresponding to `gp`.

            """
            n = len(gp)
            om = np.array([[(r==abs(gp[c]))*np.sign(gp[c]) for c in range(n)]
                for r in range(n)])
            return om

        @staticmethod
        def matrixImage(gp, chi):
            """ Image of a matrix under a group element.

            Parameters
            ----------
            gp : array
                Group element.
            chi : ndarray
                Matrix.

            Returns
            -------
            zeta : ndarray
                Image of `chi` under `gp`.
            
            """
            d = ch.shape[0]
            zeta = chi.copy()
            minusSign = np.where(gp<0)[0]
            if len(minusSign)>0:
                zeta[minusSign,:] *= -1
                zeta[:,minusSign] *= -1
            return zeta[np.abs(gp),np.abs(gp)]

        @staticmethod
        def operatorsImage(gp, X):
            """ Image of a list of operator under a group element.

            Parameters
            ----------
            gp : array
                Group element.
            X : list
                List of operator (cf. e.g. RAC22 sampleOperators method).

            Returns
            -------
            Y : ndarray
                Image of `X` under `gp`.
            
            """
            assert len(gp) == len(X), ( 
                    "Operators and gp should have the same length.")
            Y = [np.sign(GenPerm.image(gp,i))*x for i,x in enumerate(X)]
            return Y

        @staticmethod
        def image(gp, i):
            s = np.sign(i)
            absI = abs(i)
            j = gp[absI]*s
            return j

        @staticmethod
        def findMovedPoint(gp):
            for i,g in enumerate(gp):
                if i != g:
                    j=i
                    return j

        @staticmethod
        def fromCycles(n, *args):
            p = np.arange(n)
            for cycle in args[::-1]:
                cycleImage = cycle[1:]+[cycle[0]] 
                newEl = np.arange(n)
                newEl = newEl[cycle]
                p = GenPerm.compose(newEl,p)

        @staticmethod
        def compose(x, y):
            assert len(x) == len(y), (
                    "arg should be of same length")
            z = [GenPerm.image(x, GenPerm.image(y,i)) for i in range(len(x))]
            z = np.array(z)
            return z

        @staticmethod
        def inverse(x):
            n = len(x)
            xAbs = np.abs(x)
            y = np.arange(n)[xAbs]
            flip = np.where(x<0)[0]
            invFlip = xAbs[flip]
            y[invFlip] *= -1
            return y

        @staticmethod
        def random(n):
            x = np.random.permutation(n)
            xFlip = np.array([_ if np.random.rand() < .5 else -_ for _ in x])
            return xFlip

